//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import Foundation
import CoreLocation

class WeatherViewModel: ObservableObject {
    let networkService: WeatherServiceDelegate
    var lastLocation: CLLocation?
    @Published var searchText: String = ""
    @Published var isLoadResult: Bool = false
    @Published var weatherDetails: WeatherModel?
    
    
    init(networkService: WeatherServiceDelegate = WeatherNetworkService()) {
        self.networkService = networkService
        self.enableLocation()
    }
    
    func fetchWeatherDetails() {
        networkService.getWetherData(param: parametersForWeatherData()) {response in
            switch response {
            case .success(let weatherData):
                print("Fetched weather Details")
                self.weatherDetails = weatherData
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func parametersForWeatherData() -> [String: String] {
        if searchText.isEmpty {
            return [WebAPIParam.lat: "\(String(describing: lastLocation?.coordinate.latitude))", 
                    WebAPIParam.lon: "\(String(describing: lastLocation?.coordinate.longitude))"]
        } else {
            return[WebAPIParam.city: searchText]
        }
    }
}

extension WeatherViewModel: LocationServiceDelegate {
    private func enableLocation() {
        LocationManager.shared.checkLocationService()
        LocationManager.shared.locationServiceDelegate = self
    }
    
    func tracingLocationUpdate(updatedLocation: [String : Any]) {
        guard let location = updatedLocation["location"] as? CLLocation else { return }
        lastLocation = location
        fetchWeatherDetails()
    }
}
