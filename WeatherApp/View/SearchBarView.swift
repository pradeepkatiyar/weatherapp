//
//  SearchBarView.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import SwiftUI

struct SearchBarView: View {
    @Binding var searchText: String
    @Binding var showResult: Bool
    @State var showClearButton = false
    
    var placeholder = "Search"
    
    var body: some View {
        TextField(placeholder, text: $searchText, onEditingChanged: { editing in
            showClearButton = editing
            showResult = false
        }, onCommit: {
            showClearButton = false
            showResult = false
        })
        .onSubmit {
            showResult = true
        }
        .modifier(ClearButton(text: $searchText, isVisible: $showClearButton))
        .padding(.horizontal)
        .padding(.vertical, 10)
        .background(Color(.secondarySystemBackground))
        .cornerRadius(12)
        .accessibilityIdentifier(Constants.AccessbilityIdentifier.searchTextField)
        .accessibilityLabel(Constants.AccessbilityLabel.searchView)
    }
    
    struct ClearButton: ViewModifier {
        @Binding var text: String
        @Binding var isVisible: Bool
        
        func body(content: Content) -> some View {
            HStack {
                content
                Spacer()
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(Color(.placeholderText))
                    .opacity(!text.isEmpty ? 1 : 0)
                    .opacity(isVisible ? 1 : 0)
                    .onTapGesture { self.text = "" }
                .accessibilityIdentifier(Constants.AccessbilityIdentifier.searchClearButton)
                .accessibilityLabel(Constants.AccessbilityLabel.searchClearButton)
            }
        }
    }
}

#Preview {
    SearchBarView(searchText: .constant(""), showResult: .constant(false))
}
