//
//  MainWidgetView.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import SwiftUI

struct MainWidgetView: View {
    @StateObject var viewModel: WeatherViewModel
    
    var body: some View {
        if let result = viewModel.weatherDetails {
            VStack(spacing: Constants.Dimensions.firstSpacing) {
                RemoteImage(url: "\(Api.imgURL + (result.weather.first?.icon ?? "sun") + ".png")")
                HStack(spacing: Constants.Dimensions.secondSpacing) {
                    VStack(alignment: .center) {
                        Text("\(result.main?.temp ?? 0.0, specifier: "%.2f")°C" )
                            .font(.system(size: Constants.Font.largeSize))
                        Text("\(result.weather.first?.main ?? "--")")
                            .font(.system(size: Constants.Font.mediumSize))
                    }
                }
                .padding()
                VStack(spacing: Constants.Dimensions.defaultPadding) {
                    HStack {
                        Spacer()
                        WidgetView(text: Constants.Strings.windSpeed,
                                   title: "\(result.wind?.speed ?? 0.0)")
                        Spacer()
                        WidgetView(text: Constants.Strings.humidity,
                                   title: "\(result.main?.humidity ?? 0)")
                        Spacer()
                        WidgetView(text: Constants.Strings.description,
                                   title: "\(result.weather.first?.description ?? "--")")
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        WidgetView(text: Constants.Strings.feelsLike,
                                   title: "\(result.main?.feels_like ?? 0.0)°C")
                        Spacer()
                        WidgetView(text: Constants.Strings.maxTemp,
                                   title: "\(result.main?.temp_max ?? 0.0)")
                        Spacer()
                        WidgetView(text: Constants.Strings.minTemp,
                                   title: "\(result.main?.temp_min ?? 0.0)")
                        Spacer()
                    }
                }
            }
            .frame(maxWidth: .infinity)
            .padding(Constants.Dimensions.defaultPadding)
            .foregroundStyle(.white)
            .background(
                RoundedRectangle(cornerRadius: Constants.Dimensions.cornerRadius)
                    .fill(
                        LinearGradient(
                            gradient: Gradient(colors: Constants.Colors.gradient),
                            startPoint: .topLeading,
                            endPoint: .bottomTrailing
                        )
                    )
            )
            .shadow(color: Color.white.opacity(0.1), radius: 2, x: -2, y: -2)
            .shadow(color: Color.black.opacity(0.2), radius: 2, x: 2, y: 2)
            .accessibilityIdentifier(Constants.AccessbilityIdentifier.weatherDetailView)
            .accessibilityLabel(Constants.AccessbilityLabel.weatherDescription)
        } else {
            ProgressView()
                .onAppear(perform: {
                    viewModel.fetchWeatherDetails()
                })
        }
    }
    
    private func WidgetView (text: String, title: String) -> some View {
        VStack {
            Text(text)
            Text(title)
        }
        .font(.system(size: Constants.Font.smallSize))
        .accessibilityIdentifier(Constants.AccessbilityIdentifier.weatherDetailView)
        .accessibilityLabel(Constants.AccessbilityLabel.weatherDescription)
    }
}

struct RemoteImage: View {
    @StateObject private var imageLoader: ImageLoader
    
    init(url: String) {
        _imageLoader = StateObject(wrappedValue: ImageLoader(url: url))
    }
    
    var body: some View {
        if let image = imageLoader.image {
            Image(uiImage: image)
                .resizable()
                .frame(width: (CGFloat(3)*(Constants.Dimensions.defaultWidth)),
                       height: (CGFloat(3)*(Constants.Dimensions.defaultHeight)),
                       alignment: .center)
        } else {
            ProgressView()
        }
    }
}

