//
//  MockNetworkService.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/8/24.
//

import Foundation

// Mock NetworkService
class MockNetworkService: WeatherServiceDelegate {
    
    var result: Result<Decodable, NetworkError>?
    func getWetherData(param: [String : String], completion: @escaping (Result<WeatherModel, NetworkError>) -> Void) {
        if let result = result {
            switch result {
            case .success(let response):
                if let response = response as? WeatherModel {
                    completion(.success(response))
                } else {
                    completion(.failure(.badData))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        } else {
            completion(.failure(.invalidStatusCode(502)))
        }
    }
}
