//
//  ImageLoader.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import SwiftUI
import Combine

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    private var cancellables = Set<AnyCancellable>()
    private var url: String
    
    init(url: String) {
        self.url = url
        loadImage()
    }
    
    private func loadImage() {
        if let cachedImage = ImageCache.shared.get(forKey: url) {
            self.image = cachedImage
            return
        }
        
        guard let url = URL(string: url) else {
            print("Invalid URL: \(self.url)")
            return
        }
        
        URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .map(UIImage.init(data:))
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print("Failed to load image: \(error.localizedDescription)")
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] image in
                guard let self = self, let image = image else {
                    print("No image data received.")
                    return
                }
                self.image = image
                ImageCache.shared.set(image, forKey: self.url)
            })
            .store(in: &cancellables)
    }
}
