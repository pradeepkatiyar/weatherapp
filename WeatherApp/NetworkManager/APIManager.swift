//
//  APIManager.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import Foundation

// MARK: - Network API Path

enum Api {
    static let baseURL = "https://api.openweathermap.org/data/2.5/weather"
    static let imgURL = "https://openweathermap.org/img/wn/"
    static let apiKey = "be971f5c6b8c865e26719f42ebf234a5"
    
    enum APIParam: String {
        case appId = "appid"
        case lat = "lat"
        case lon = "lon"
        case city = "q"
    }
}

//----- Web API Param -----
struct WebAPIParam {
    static let appid = "appid"
    static let lat = "lat"
    static let lon = "lon"
    static let city = "q"
}
