//
//  WeatherNetworkService.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import Foundation


protocol WeatherServiceDelegate {
    func getWetherData(param: [String: String], completion: @escaping(Result<WeatherModel, NetworkError>) -> Void)
}

class WeatherNetworkService: WeatherServiceDelegate  {
    
    func getWetherData(param: [String: String], completion: @escaping(Result<WeatherModel, NetworkError>) -> Void) {
        var params = [WebAPIParam.appid: Api.apiKey]
        if !param.isEmpty {
            params.merge(param){ (current, _) in current }
        }
        guard let url = URL(string: Api.baseURL)?.appendingParams(params: params) else {
            return completion(.failure(.invalidURL))
        }
        NetworkManager().request(url: url, headers: params, completion: completion)
    }
}

private extension URL {
    func appendingParams(params: [String: String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: false)
        components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }

        return components?.url
    }
}
