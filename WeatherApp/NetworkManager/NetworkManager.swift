//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import Foundation

protocol NetworkServices {
    func request<T: Decodable>(
        url: URL?,
        method: String,
        headers: [String: String],
        body: Data?,
        session: URLSession,
        completion: @escaping (Result<T, NetworkError>) -> Void)
}

enum NetworkError: Error {
    case invalidURL
    case invalidStatusCode(Int)
    case badData
    case decodeError(Error)
    case other(Error)
    case invalidResponse
    case invalidContentType(String)
}

class NetworkManager: NetworkServices {
    
    private let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }

    func request<T>(
        url: URL?,
        method: String = "GET",
        headers: [String: String] = [:],
        body: Data? = nil,
        session: URLSession = .shared,
        completion: @escaping (Result<T, NetworkError>) -> Void) where T: Decodable {
            
            guard let url = url else {
                completion(.failure(.invalidURL))
                return
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = method
            request.allHTTPHeaderFields = headers
            request.httpBody = body
            
            print("Requesting URL: \(url)")
            print("Method: \(method)")
            print("Headers: \(headers)")
            
            session.dataTask(with: request) { data, response, error in
                
                if let error = error {
                    completion(.failure(.other(error)))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(.failure(.invalidResponse))
                    return
                }
                
                guard (200..<300).contains(httpResponse.statusCode) else {
                    completion(.failure(.invalidStatusCode(httpResponse.statusCode)))
                    return
                }
                
                guard let data = data, !data.isEmpty else {
                    completion(.failure(.badData))
                    return
                }
                
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(model))
                } catch {
                     completion(.failure(.decodeError(error)))
                }
            }.resume()
        }
}
