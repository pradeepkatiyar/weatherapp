//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import CoreLocation

protocol LocationServiceDelegate {
    func tracingLocationUpdate(updatedLocation: [String: Any])
}

// Location manager
class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    
    let manager = CLLocationManager()
    var locationServiceDelegate: LocationServiceDelegate?
    
    private override init() {
        super.init()
        self.setupLocationManager()
    }
    
    private func setupLocationManager() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationService() {
        Task { [weak self] in
            guard let self else { return }
            let isLocationEnabled = await self.locationServicesEnabled()
            if isLocationEnabled {
                self.checkLocationManagerAuthorization()
            } else  {
                self.updateLocation(updatedLocation: ["error": true])
            }
        }
    }
    
    private func locationServicesEnabled() async -> Bool {
        CLLocationManager.locationServicesEnabled()
    }
    
    private func checkLocationManagerAuthorization() {
        switch authorizationStatus() {
        case .notDetermined:
            print("Auth: notDetermined")
            manager.requestWhenInUseAuthorization()
            
        case .authorizedAlways, .authorizedWhenInUse:
            print("Auth: authorizedWhenInUse")
            manager.startUpdatingLocation()
            
        case .denied, .restricted:
            print("Auth: denied")
            updateLocation(updatedLocation: ["error": true])
            break
        default:
            updateLocation(updatedLocation: ["error": true])
            break
        }
    }
    
    private func authorizationStatus() -> CLAuthorizationStatus {
        var status: CLAuthorizationStatus
        
        if #available(iOS 14.0, *) {
            status = CLLocationManager().authorizationStatus
        } else {
            status = CLLocationManager.authorizationStatus()
        }
        
        return status
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let object: [String: Any] = [
                "error": false,
                "location": location
            ]
            updateLocation(updatedLocation: object)
            manager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationService()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
    }

    // Notify Update location
    private func updateLocation(updatedLocation: [String: Any]){
        locationServiceDelegate?.tracingLocationUpdate(updatedLocation: updatedLocation)
    }
}
