//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            WeatherView()
        }
    }
}
