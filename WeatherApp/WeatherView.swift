//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Pradeep Katiyar on 11/7/24.
//

import SwiftUI

struct WeatherView: View {
    @StateObject var viewModel = WeatherViewModel()
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Text("Weather")
                    .font(.system(size: 22, weight: .bold))
                    .padding()
                
                ScrollView(showsIndicators: false) {
                    SearchBarView(searchText: $viewModel.searchText, showResult: $viewModel.isLoadResult, placeholder: "Search for a city")
                        .padding()
                    
                    if viewModel.isLoadResult {
                        MainWidgetView(viewModel: viewModel)
                            .padding()
                    }
                }
                
                Spacer()
            }
            .background(
                LinearGradient(
                    gradient: Gradient(colors: Constants.Colors.gradientAPP),
                    startPoint: .topLeading, endPoint: .bottomTrailing))
            Spacer()
        }
    }
}

#Preview {
    WeatherView()
}
