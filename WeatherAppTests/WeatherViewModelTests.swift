//
//  WeatherViewModelTests.swift
//  WeatherAppTests
//
//  Created by Pradeep Katiyar on 11/8/24.
//

import XCTest
@testable import WeatherApp

final class WeatherViewModelTests: XCTestCase {
    var viewModel: WeatherViewModel!
    var mockNetworkService: MockNetworkService!
    
    override func setUp() {
        super.setUp()
        mockNetworkService = MockNetworkService()
        viewModel = WeatherViewModel(networkService: mockNetworkService)
    }
    
    override func tearDown() {
        viewModel = nil
        mockNetworkService = nil
        super.tearDown()
    }
    
    
    private func readJSONFromFile<T: Codable>(fileName: String, type: T.Type) -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(T.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    func testFetchWeatherDetailsSuccess() {
        // Given
        mockNetworkService.result = .success(readJSONFromFile(fileName: "WeatherResponse", type: WeatherModel.self))
        let expectation = self.expectation(description: "Network request should be made")
        
        // When
        viewModel.searchText = "San Francisco"
        viewModel.fetchWeatherDetails()
        
        // Then
        XCTAssertEqual(viewModel.weatherDetails?.name, "San Francisco")
        XCTAssertEqual(viewModel.weatherDetails?.weather.first?.main, "Clouds")
        XCTAssertEqual(viewModel.weatherDetails?.weather.first?.description, "few clouds")
        XCTAssertEqual(viewModel.weatherDetails?.weather.first?.icon, "02d")
        expectation.fulfill()
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testFetchWeatherDetailsFailed() {
        // Given
        let error = NetworkError.badData
        mockNetworkService.result = .failure(error)
        let expectation = self.expectation(description: "Network request should handle error")
        
        // When
        viewModel.searchText = "InvalidCity" // Trigger request
        
        // Then
        XCTAssertNil(viewModel.weatherDetails)
        expectation.fulfill()
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
}
